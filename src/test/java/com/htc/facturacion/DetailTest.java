package com.htc.facturacion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.facturacion.dao.BillDao;
import com.htc.facturacion.dao.CustomerDao;
import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.model.DetailModel;
import com.htc.facturacion.service.BillService;
import com.htc.facturacion.service.CustomerService;
import com.htc.facturacion.service.DetailService;
import com.htc.facturacion.service.ProductService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DetailTest {
	
	@Autowired
	DetailService detailService;
	@Autowired
	DetailDao detailDao;
	@Autowired
	BillService billService;
	@Autowired
	BillDao billDao;
	@Autowired
	ProductService productService;
	@Autowired
	ProductDao productDao;
	@Autowired
	CustomerService customerService;
	@Autowired
	CustomerDao customerDao;

	/*@Test
	public void insert() throws Exception {
		
		BillModel bill = new BillModel();
		Date fecha = new Date();
		 
		bill.setFacturaId(2);
		bill.setIdCustomer(1);
		bill.setIva(0.13);
		bill.setSubtotal(900.00);
		bill.setTotal(1000);
		bill.setFecha(fecha);
		 
		 
		DetailModel detail = new DetailModel();
		 
		 detail.setIdDetail(2);
		 detail.setIdBill(2);
		 detail.setIdProduct(2);
		 detail.setCantidad(2);
		 detail.setSubTotal(750.00);
		 
		 
		 assertEquals(true,  detailService.insert(detail));
	}

	/*
	@Test
	public void update() {
		
		Bill bill_2 = new Bill();
		Date fecha = new Date();
		 
		 bill_2.setFacturaId(2);
		 bill_2.setDescripcion("compra vehiculo Nissan");
		 bill_2.setObservacion("pago contado");
		 bill_2.setFechaCreacion(fecha);
		
		Detail detail_2 = new Detail();
		 
		 detail_2.setDetalleId(2);
		 detail_2.setProducto("Honda Civic SI");
		 detail_2.setCantidad(1);
		 detail_2.setPrecio(40000.00);
		 detail_2.setFacturaId(bill_2);;
		 detail_2.setTotal(40000.00);
		 
		 
		 assertEquals(true,  detailService.updateDetail(detail_2));
		
	}
	
	@Test
	public void delete() {
		Detail detail = new Detail();
		detail.setDetalleId(2);
		assertEquals(true,  detailService.deleteDetail(detail));
	}*/
	
	@Test
	public void leerTodo() throws Exception {
		
		assertNotNull(detailService.loadAllDetails());
	}
	
	/*@Test
	public void leerById() {
		assertNotNull(detailService.getDetalleById(1));
	}*/
}
