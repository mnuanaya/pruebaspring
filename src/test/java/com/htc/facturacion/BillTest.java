package com.htc.facturacion;

import static org.junit.Assert.assertEquals;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.service.BillService;
import com.htc.facturacion.service.DetailService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BillTest {

	@Autowired
	 BillService billService;
	@Autowired
	DetailService detailService;
	
	
	@Test
	public void insert() throws Exception {
		
		 
		 
		
		
		java.util.Date fecha = new java.util.Date();
		java.sql.Date fechasql = new java.sql.Date(fecha.getTime());
		BillModel bill = new BillModel();

		
		List<com.htc.facturacion.model.DetailModel> details = new ArrayList<>();
		details.add(detailService.getDetail(2, 1));
		details.add(detailService.getDetail(2, 1));
		
		bill.setIdCustomer(1);
		bill.setDetails(details);
		bill.setFecha(fechasql);
		
		 
		 
		
		 assertEquals(true, billService.insert(bill));
	}
	
	
	
	@Test
	public void delete() throws OperationException {
		
		BillModel bill = new BillModel();
		bill.setFacturaId(64329834);
		
		assertEquals(true,billService.deleteBill(bill));
		
	}
	
	@Test
	public void buscarTodos() throws EmptyException {
		
		assertEquals(true,billService.loadAllBills());
	}
	
	@Test
	public void buscarId() throws NullObjectException {
		
		
		
		assertEquals(true,billService.getFacturaById(24));
		
	}

}
