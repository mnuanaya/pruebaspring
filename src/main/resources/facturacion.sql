PGDMP                          w         	   facturabd    10.2    10.2 &               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       1262    24616 	   facturabd    DATABASE     �   CREATE DATABASE facturabd WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_El Salvador.1252' LC_CTYPE = 'Spanish_El Salvador.1252';
    DROP DATABASE facturabd;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false                       0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false                       0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    24643    bill    TABLE     �   CREATE TABLE bill (
    id_bill integer NOT NULL,
    id_customer integer NOT NULL,
    iva double precision NOT NULL,
    subtotal double precision NOT NULL,
    total double precision NOT NULL,
    fecha date NOT NULL
);
    DROP TABLE public.bill;
       public         postgres    false    3            �            1259    24641    bill_id_bill_seq    SEQUENCE     �   CREATE SEQUENCE bill_id_bill_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.bill_id_bill_seq;
       public       postgres    false    201    3                       0    0    bill_id_bill_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE bill_id_bill_seq OWNED BY bill.id_bill;
            public       postgres    false    200            �            1259    24619    customer    TABLE     �   CREATE TABLE customer (
    id_customer integer NOT NULL,
    nombre character varying(200) NOT NULL,
    apellido character varying(200) NOT NULL
);
    DROP TABLE public.customer;
       public         postgres    false    3            �            1259    24617    customer_id_customer_seq    SEQUENCE     �   CREATE SEQUENCE customer_id_customer_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.customer_id_customer_seq;
       public       postgres    false    197    3                       0    0    customer_id_customer_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE customer_id_customer_seq OWNED BY customer.id_customer;
            public       postgres    false    196            �            1259    24656    detail    TABLE     �   CREATE TABLE detail (
    id_detail integer NOT NULL,
    id_bill integer NOT NULL,
    id_product integer NOT NULL,
    cantidad integer NOT NULL,
    subtotal double precision NOT NULL
);
    DROP TABLE public.detail;
       public         postgres    false    3            �            1259    24654    detail_id_detail_seq    SEQUENCE     �   CREATE SEQUENCE detail_id_detail_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.detail_id_detail_seq;
       public       postgres    false    203    3                       0    0    detail_id_detail_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE detail_id_detail_seq OWNED BY detail.id_detail;
            public       postgres    false    202            �            1259    24627    product    TABLE     �   CREATE TABLE product (
    id_product integer NOT NULL,
    product_name character varying(200) NOT NULL,
    description character varying(200) NOT NULL,
    unit_price double precision NOT NULL
);
    DROP TABLE public.product;
       public         postgres    false    3            �            1259    24625    product_id_product_seq    SEQUENCE     �   CREATE SEQUENCE product_id_product_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.product_id_product_seq;
       public       postgres    false    199    3                       0    0    product_id_product_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE product_id_product_seq OWNED BY product.id_product;
            public       postgres    false    198            �
           2604    24646    bill id_bill    DEFAULT     ^   ALTER TABLE ONLY bill ALTER COLUMN id_bill SET DEFAULT nextval('bill_id_bill_seq'::regclass);
 ;   ALTER TABLE public.bill ALTER COLUMN id_bill DROP DEFAULT;
       public       postgres    false    201    200    201            �
           2604    24622    customer id_customer    DEFAULT     n   ALTER TABLE ONLY customer ALTER COLUMN id_customer SET DEFAULT nextval('customer_id_customer_seq'::regclass);
 C   ALTER TABLE public.customer ALTER COLUMN id_customer DROP DEFAULT;
       public       postgres    false    197    196    197            �
           2604    24659    detail id_detail    DEFAULT     f   ALTER TABLE ONLY detail ALTER COLUMN id_detail SET DEFAULT nextval('detail_id_detail_seq'::regclass);
 ?   ALTER TABLE public.detail ALTER COLUMN id_detail DROP DEFAULT;
       public       postgres    false    202    203    203            �
           2604    24630    product id_product    DEFAULT     j   ALTER TABLE ONLY product ALTER COLUMN id_product SET DEFAULT nextval('product_id_product_seq'::regclass);
 A   ALTER TABLE public.product ALTER COLUMN id_product DROP DEFAULT;
       public       postgres    false    198    199    199                      0    24643    bill 
   TABLE DATA               J   COPY bill (id_bill, id_customer, iva, subtotal, total, fecha) FROM stdin;
    public       postgres    false    201   �'       	          0    24619    customer 
   TABLE DATA               :   COPY customer (id_customer, nombre, apellido) FROM stdin;
    public       postgres    false    197   �'                 0    24656    detail 
   TABLE DATA               M   COPY detail (id_detail, id_bill, id_product, cantidad, subtotal) FROM stdin;
    public       postgres    false    203   (                 0    24627    product 
   TABLE DATA               M   COPY product (id_product, product_name, description, unit_price) FROM stdin;
    public       postgres    false    199   .(                  0    0    bill_id_bill_seq    SEQUENCE SET     8   SELECT pg_catalog.setval('bill_id_bill_seq', 1, false);
            public       postgres    false    200                       0    0    customer_id_customer_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('customer_id_customer_seq', 1, false);
            public       postgres    false    196                       0    0    detail_id_detail_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('detail_id_detail_seq', 1, false);
            public       postgres    false    202                       0    0    product_id_product_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('product_id_product_seq', 1, false);
            public       postgres    false    198            �
           2606    24648    bill bill_pkey 
   CONSTRAINT     J   ALTER TABLE ONLY bill
    ADD CONSTRAINT bill_pkey PRIMARY KEY (id_bill);
 8   ALTER TABLE ONLY public.bill DROP CONSTRAINT bill_pkey;
       public         postgres    false    201            �
           2606    24624    customer customer_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY customer
    ADD CONSTRAINT customer_pkey PRIMARY KEY (id_customer);
 @   ALTER TABLE ONLY public.customer DROP CONSTRAINT customer_pkey;
       public         postgres    false    197            �
           2606    24661    detail detail_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY detail
    ADD CONSTRAINT detail_pkey PRIMARY KEY (id_detail);
 <   ALTER TABLE ONLY public.detail DROP CONSTRAINT detail_pkey;
       public         postgres    false    203            �
           2606    24632    product product_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id_product);
 >   ALTER TABLE ONLY public.product DROP CONSTRAINT product_pkey;
       public         postgres    false    199            �
           2606    24662    detail id_bill_fk    FK CONSTRAINT     �   ALTER TABLE ONLY detail
    ADD CONSTRAINT id_bill_fk FOREIGN KEY (id_bill) REFERENCES bill(id_bill) ON UPDATE CASCADE ON DELETE CASCADE;
 ;   ALTER TABLE ONLY public.detail DROP CONSTRAINT id_bill_fk;
       public       postgres    false    203    201    2697            �
           2606    24649    bill id_customer_fk    FK CONSTRAINT     �   ALTER TABLE ONLY bill
    ADD CONSTRAINT id_customer_fk FOREIGN KEY (id_customer) REFERENCES customer(id_customer) ON UPDATE CASCADE ON DELETE CASCADE;
 =   ALTER TABLE ONLY public.bill DROP CONSTRAINT id_customer_fk;
       public       postgres    false    2693    201    197            �
           2606    24667    detail id_product_fk    FK CONSTRAINT     �   ALTER TABLE ONLY detail
    ADD CONSTRAINT id_product_fk FOREIGN KEY (id_product) REFERENCES product(id_product) ON UPDATE CASCADE ON DELETE CASCADE;
 >   ALTER TABLE ONLY public.detail DROP CONSTRAINT id_product_fk;
       public       postgres    false    2695    199    203                  x������ � �      	      x������ � �            x������ � �            x������ � �     