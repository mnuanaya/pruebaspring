CREATE TABLE public.bill
(
    factura_id integer NOT NULL DEFAULT nextval('factura_factura_id_seq'::regclass),
    descripcion character varying(200) COLLATE pg_catalog."default" NOT NULL,
    observacion character varying(200) COLLATE pg_catalog."default",
    fecha_creacion date NOT NULL,
    CONSTRAINT factura_pkey PRIMARY KEY (factura_id)
)

CREATE TABLE public.detail
(
    detalle_id integer NOT NULL DEFAULT nextval('detalle_detalle_id_seq'::regclass),
    producto character varying(250) COLLATE pg_catalog."default" NOT NULL,
    cantidad integer NOT NULL,
    precio double precision NOT NULL,
    factura_id integer NOT NULL,
    total double precision NOT NULL,
    CONSTRAINT detalle_pk PRIMARY KEY (detalle_id),
    CONSTRAINT factura_fk FOREIGN KEY (factura_id)
        REFERENCES public.bill (factura_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
)