//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.31 a las 09:04:30 PM CST 
//


package com.htc.facturacion.webs;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.htc.facturacion.webs package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.htc.facturacion.webs
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link InsertBillRequest }
     * 
     */
    public InsertBillRequest createInsertBillRequest() {
        return new InsertBillRequest();
    }

    /**
     * Create an instance of {@link DetailGet }
     * 
     */
    public DetailGet createDetailGet() {
        return new DetailGet();
    }

    /**
     * Create an instance of {@link InsertBillResponse }
     * 
     */
    public InsertBillResponse createInsertBillResponse() {
        return new InsertBillResponse();
    }

    /**
     * Create an instance of {@link Response }
     * 
     */
    public Response createResponse() {
        return new Response();
    }

    /**
     * Create an instance of {@link GetBillResponse }
     * 
     */
    public GetBillResponse createGetBillResponse() {
        return new GetBillResponse();
    }

    /**
     * Create an instance of {@link Bill }
     * 
     */
    public Bill createBill() {
        return new Bill();
    }

    /**
     * Create an instance of {@link GetBillRequest }
     * 
     */
    public GetBillRequest createGetBillRequest() {
        return new GetBillRequest();
    }

    /**
     * Create an instance of {@link GetBillByIdResponse }
     * 
     */
    public GetBillByIdResponse createGetBillByIdResponse() {
        return new GetBillByIdResponse();
    }

    /**
     * Create an instance of {@link AnularBillResponse }
     * 
     */
    public AnularBillResponse createAnularBillResponse() {
        return new AnularBillResponse();
    }

    /**
     * Create an instance of {@link GetBillByIdRequest }
     * 
     */
    public GetBillByIdRequest createGetBillByIdRequest() {
        return new GetBillByIdRequest();
    }

    /**
     * Create an instance of {@link AnularBillRequest }
     * 
     */
    public AnularBillRequest createAnularBillRequest() {
        return new AnularBillRequest();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link Detail }
     * 
     */
    public Detail createDetail() {
        return new Detail();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

}
