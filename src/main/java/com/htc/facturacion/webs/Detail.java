//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.31 a las 09:04:30 PM CST 
//


package com.htc.facturacion.webs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para detail complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="detail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_detail" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_bill" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id_product" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="cantidad" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="subtotal" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "detail", propOrder = {
    "idDetail",
    "idBill",
    "idProduct",
    "cantidad",
    "subtotal"
})
public class Detail {

    @XmlElement(name = "id_detail")
    protected int idDetail;
    @XmlElement(name = "id_bill")
    protected int idBill;
    @XmlElement(name = "id_product")
    protected int idProduct;
    protected int cantidad;
    protected double subtotal;

    /**
     * Obtiene el valor de la propiedad idDetail.
     * 
     */
    public int getIdDetail() {
        return idDetail;
    }

    /**
     * Define el valor de la propiedad idDetail.
     * 
     */
    public void setIdDetail(int value) {
        this.idDetail = value;
    }

    /**
     * Obtiene el valor de la propiedad idBill.
     * 
     */
    public int getIdBill() {
        return idBill;
    }

    /**
     * Define el valor de la propiedad idBill.
     * 
     */
    public void setIdBill(int value) {
        this.idBill = value;
    }

    /**
     * Obtiene el valor de la propiedad idProduct.
     * 
     */
    public int getIdProduct() {
        return idProduct;
    }

    /**
     * Define el valor de la propiedad idProduct.
     * 
     */
    public void setIdProduct(int value) {
        this.idProduct = value;
    }

    /**
     * Obtiene el valor de la propiedad cantidad.
     * 
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Define el valor de la propiedad cantidad.
     * 
     */
    public void setCantidad(int value) {
        this.cantidad = value;
    }

    /**
     * Obtiene el valor de la propiedad subtotal.
     * 
     */
    public double getSubtotal() {
        return subtotal;
    }

    /**
     * Define el valor de la propiedad subtotal.
     * 
     */
    public void setSubtotal(double value) {
        this.subtotal = value;
    }

}
