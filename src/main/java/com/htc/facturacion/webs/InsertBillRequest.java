//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.31 a las 09:04:30 PM CST 
//


package com.htc.facturacion.webs;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id_customer" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="detailGet" type="{http://webs.facturacion.htc.com}detailGet" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "idCustomer",
    "detailGet"
})
@XmlRootElement(name = "insertBillRequest")
public class InsertBillRequest {

    @XmlElement(name = "id_customer")
    protected int idCustomer;
    @XmlElement(required = true)
    protected List<DetailGet> detailGet;

    /**
     * Obtiene el valor de la propiedad idCustomer.
     * 
     */
    public int getIdCustomer() {
        return idCustomer;
    }

    /**
     * Define el valor de la propiedad idCustomer.
     * 
     */
    public void setIdCustomer(int value) {
        this.idCustomer = value;
    }

    /**
     * Gets the value of the detailGet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the detailGet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDetailGet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DetailGet }
     * 
     * 
     */
    public List<DetailGet> getDetailGet() {
        if (detailGet == null) {
            detailGet = new ArrayList<DetailGet>();
        }
        return this.detailGet;
    }

}
