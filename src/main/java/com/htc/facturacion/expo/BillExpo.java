package com.htc.facturacion.expo;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.webs.AnularBillResponse;
import com.htc.facturacion.webs.GetBillByIdResponse;
import com.htc.facturacion.webs.GetBillResponse;
import com.htc.facturacion.webs.InsertBillResponse;

public interface BillExpo {
	
	public  InsertBillResponse InsertBillResponse(BillModel billM)throws OperationException, NullObjectException,Exception;
	public GetBillResponse getBill()throws EmptyException, Exception;
	public GetBillByIdResponse getBillById(Integer id)throws NullObjectException, Exception;
	public AnularBillResponse anularBill(Integer id) throws NullObjectException, OperationException, Exception;

}
