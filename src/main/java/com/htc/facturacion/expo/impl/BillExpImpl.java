package com.htc.facturacion.expo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.expo.BillExpo;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.service.BillService;
import com.htc.facturacion.service.DetailService;
import com.htc.facturacion.utils.Constants;
import com.htc.facturacion.webs.AnularBillResponse;
import com.htc.facturacion.webs.Bill;
import com.htc.facturacion.webs.GetBillByIdResponse;
import com.htc.facturacion.webs.GetBillResponse;
import com.htc.facturacion.webs.InsertBillResponse;

@Component
public class BillExpImpl implements BillExpo {

	@Autowired
	private BillService billService;

	@Autowired
	private DetailService detailService;

	@Autowired
	private Environment envi;

	public InsertBillResponse InsertBillResponse(BillModel bModel) throws OperationException, NullObjectException {
		InsertBillResponse response = new InsertBillResponse();

		try {

			billService.insert(bModel);

		} catch (OperationException e) {

			throw e;

		} catch (NullObjectException e) {
			throw e;

		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	public GetBillResponse getBill() throws EmptyException {
		GetBillResponse response = new GetBillResponse();

		try {

			List<Bill> bills = billService.castToDtoList(billService.loadAllBills());
			response.getBills().addAll(bills);

		} catch (EmptyException e) {

			throw e;

		} catch (Exception ex) {
			throw ex;
		}

		return response;
	}

	public GetBillByIdResponse getBillById(Integer id) throws NullObjectException, Exception {
		GetBillByIdResponse response = new GetBillByIdResponse();

		try {

			response.setBill(billService.castToDto(billService.getFacturaById(id)));

		} catch (NullObjectException e) {
			throw e;

		} catch (Exception e) {
			throw e;
		}

		return response;
	}

	public AnularBillResponse anularBill(Integer id) throws NullObjectException, OperationException, Exception {
		AnularBillResponse response = new AnularBillResponse();

		try {

			Bill b = billService.castToDto(billService.getFacturaById(id));
			BillModel bModel = billService.castToModel(b);

			if (bModel.getEstado() || b == null) {

				billService.BillNull(id, billService.castToModel(b));

			} else {
				throw new OperationException(Long.valueOf(envi.getProperty(Constants.NO_ANULAR_CODE)),
						envi.getProperty(Constants.MESSAGE_NO_ANULAR));
			}

		} catch (NullObjectException e) {
			throw e;

		} catch (OperationException e) {
			throw e;

		} catch (Exception e) {
			throw e;
		}

		return response;
	}

}
