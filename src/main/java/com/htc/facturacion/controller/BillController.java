package com.htc.facturacion.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.expo.BillExpo;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.utils.Constants;
import com.htc.facturacion.webs.Bill;
import com.htc.facturacion.webs.Response;

@RestController
@RequestMapping("/bill")
public class BillController {

	@Autowired
	BillExpo billExpo;

	@Autowired
	Environment env;

	@PostMapping("/")
	ResponseEntity<Object> InsertBill(@RequestBody BillModel billModel) {

		Response res = new Response();

		HttpHeaders headers = new HttpHeaders();

		try {
			billExpo.InsertBillResponse(billModel);
			headers.add(String.valueOf(env.getProperty(Constants.MESSAGE_SUCCESS)),
					env.getProperty(Constants.SUCCESS_CODE));

		} catch (OperationException e) {

			res.setCode(e.getCode());
			res.setMessage(e.getDescription());
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (NullObjectException e) {
			res.setCode(e.getCode());
			res.setMessage(e.getDescription());
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (Exception e) {
			res.setCode(404);
			res.setMessage("No se pudo realizar operacion, verifiquela!!");
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		}

		return new ResponseEntity<>(headers, HttpStatus.OK);
	}

	@GetMapping(path = "/")
	ResponseEntity<Object> allBill() {

		Response res = new Response();
		List<Bill> list =null;
		HttpHeaders headers = new HttpHeaders();

		try {
			
			list= billExpo.getBill().getBills();
			headers.add(String.valueOf(env.getProperty(Constants.MESSAGE_SUCCESS)),
					env.getProperty(Constants.SUCCESS_CODE));

		} catch (EmptyException e) {

			res.setCode(e.getCode());
			res.setMessage(e.getDescription());

			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (Exception ex) {
			res.setCode(404);
			res.setMessage("Imposible realizar, verifique operacion!!");

			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		}

		return new ResponseEntity<>(list,headers, HttpStatus.OK);
	}

	@GetMapping(path = "/{id}")
	ResponseEntity<Object> getBillById(@PathVariable int id) {
		Response res = new Response();
		Bill bill=null;
		HttpHeaders headers = new HttpHeaders();

		try {

			bill=billExpo.getBillById(id).getBill();
			headers.add(String.valueOf(env.getProperty(Constants.MESSAGE_SUCCESS)),
					env.getProperty(Constants.SUCCESS_CODE));

		} catch (OperationException e) {

			res.setCode(e.getCode());
			res.setMessage(e.getDescription());
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (NullObjectException e) {

			res.setCode(e.getCode());
			res.setMessage(e.getDescription());
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (Exception e) {

			res.setCode(404);
			res.setMessage("no se pudo realizar proceso");
			headers.add(String.valueOf(res.getCode()), res.getMessage());
			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));
		}

		return new ResponseEntity<>(bill,headers, HttpStatus.OK);
	}

	@DeleteMapping("/{idBill}")
	ResponseEntity<Object> anularBill(@PathVariable int idBill) {
		Response res = new Response();

		HttpHeaders headers = new HttpHeaders();

		try {

			billExpo.anularBill(idBill);
			headers.add(String.valueOf(env.getProperty(Constants.MESSAGE_SUCCESS)),
					env.getProperty(Constants.SUCCESS_CODE));

		} catch (NullObjectException e) {
			res.setCode(e.getCode());
			res.setMessage(e.getDescription());
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (OperationException e) {
			res.setCode(e.getCode());
			res.setMessage(e.getDescription());
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));

		} catch (Exception e) {
			res.setCode(404);
			res.setMessage("No se pudo realizar esta operacion, verifiquela!!");
			headers.add(String.valueOf(res.getCode()), res.getMessage());

			return new ResponseEntity<>(headers, HttpStatus.valueOf(Integer.valueOf(String.valueOf(res.getCode()))));
		}

		return new ResponseEntity<>(headers, HttpStatus.OK);
	}

}
