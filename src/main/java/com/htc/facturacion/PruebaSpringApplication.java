package com.htc.facturacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class PruebaSpringApplication {

	public static void main(String[] args) {

		SpringApplication.run(PruebaSpringApplication.class, args);
		
	}
	
	

}
