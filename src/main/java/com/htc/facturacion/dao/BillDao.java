package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.BillModel;

public interface BillDao {

	public boolean insert(BillModel bill);

	public Integer insertAndReturn(BillModel bill);

	public boolean inserBatch(List<BillModel> bills);

	public List<BillModel> loadAllBills();

	public BillModel findBillById(long factura_id);

	public boolean updateBill(Integer id, BillModel bill);

	public boolean updateStateBill(Integer id, BillModel bm);

	public boolean deleteBill(BillModel id);

}
