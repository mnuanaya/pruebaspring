package com.htc.facturacion.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.facturacion.dao.CustomerDao;
import com.htc.facturacion.model.CustomerModel;

@Repository
public class CustomerDaoImpl extends JdbcDaoSupport implements CustomerDao {

	

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(CustomerModel customer) {

		String sql = "INSERT INTO customer " + "(nombre, apellido) VALUES (?, ?)";
		return getJdbcTemplate().update(sql, new Object[] { customer.getNombre(), customer.getApellido() }) > 0;

	}

	@Transactional
	@Override
	public boolean inserBatch(List<CustomerModel> customers) {

		String sql = "INSERT INTO customer " + "(nombre,apellido) VALUES (?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				CustomerModel custom = customers.get(i);

				ps.setString(1, custom.getNombre());
				ps.setString(2, custom.getApellido());

			}

			public int getBatchSize() {
				return customers.size();
			}
		});

		return Boolean.TRUE;

	}

	@Override
	public List<CustomerModel> loadAllCustomer() {
		String sql = "SELECT * FROM customer";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<CustomerModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			CustomerModel customers = new CustomerModel();
			customers.setIdCustomer((Integer) row.get("id_customer"));
			customers.setNombre((String) row.get("nombre"));
			customers.setApellido((String) row.get("apellido"));

			result.add(customers);
		}

		return result;
	}

	@Override
	public CustomerModel findCustomerById(long cusid) {
		String sql = "SELECT * FROM customer WHERE id_customer = ?";
		List<CustomerModel> list = getJdbcTemplate().query(sql, new Object[] { cusid }, new RowMapper<CustomerModel>() {
			@Override
			public CustomerModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				CustomerModel customer = new CustomerModel();
				customer.setIdCustomer(rs.getInt("id_customer"));
				customer.setNombre(rs.getString("nombre"));
				customer.setApellido(rs.getString("apellido"));

				return customer;
			}
		});
		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public boolean updateCustomer(Integer id, CustomerModel customer) {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE customer SET nombre = ?, apellido = ? WHERE id_customer =").append(id);
		getJdbcTemplate().update(sql.toString(), new Object[] { customer.getNombre(), customer.getApellido() });

		return Boolean.TRUE;
	}

	@Override
	public boolean deleteCustomer(CustomerModel id) {

		String sql = "DELETE FROM customer WHERE id_customer = ?";
		getJdbcTemplate().update(sql, new Object[] { id.getIdCustomer() });

		return Boolean.TRUE;
	}

	@Override
	public Integer insertAndReturn(CustomerModel customer) {
		final String INSERTSQL = "INSERT INTO customer " + "(nombre, apellido) VALUES (?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERTSQL, new String[] { "id_customer" });

				ps.setString(1, customer.getNombre());
				ps.setString(2, customer.getApellido());
				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key
	}

}
