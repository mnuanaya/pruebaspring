package com.htc.facturacion.dao.impl;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.facturacion.dao.BillDao;
import com.htc.facturacion.model.BillModel;

@Repository
public class BillDaoImpl extends JdbcDaoSupport implements BillDao {

	

	@Autowired
	public DataSource dataSource;

	@Autowired
	public Environment env;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(BillModel bill) {

		String sql = "INSERT INTO bill "
				+ "(id_bill, id_customer,iva, subtotal, total, fecha, observacion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		return getJdbcTemplate().update(sql, new Object[] { bill.getFacturaId(), bill.getIdCustomer(), bill.getIva(),
				bill.getSubtotal(), bill.getTotal(), bill.getFecha(), bill.getObservacion(), bill.getEstado()

		}) > 0;

	}

	@Transactional
	@Override
	public boolean inserBatch(List<BillModel> bills) {

		String sql = "INSERT INTO bill "
				+ "(id_bill, id_customer,iva, subtotal, total, fecha, observacion, estado) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				BillModel bill = bills.get(i);
				ps.setLong(1, bill.getFacturaId());
				ps.setInt(2, bill.getIdCustomer());
				ps.setDouble(3, bill.getIva());
				ps.setDouble(4, bill.getSubtotal());
				ps.setDouble(5, bill.getTotal());
				ps.setDate(6, (Date) bill.getFecha());
				ps.setString(7, bill.getObservacion());
				ps.setBoolean(8, bill.getEstado());

			}

			public int getBatchSize() {
				return bills.size();
			}
		});

		return Boolean.TRUE;

	}

	@Override
	public List<BillModel> loadAllBills() {
		String sql = "SELECT * FROM bill";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<BillModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			BillModel bills = new BillModel();
			bills.setFacturaId((Integer) row.get("id_bill"));
			bills.setIdCustomer((Integer) row.get("id_customer"));
			bills.setIva((Double) row.get("iva"));
			bills.setSubtotal((Double) row.get("subtotal"));
			bills.setTotal((Double) row.get("total"));
			bills.setFecha((Date) row.get("fecha"));
			bills.setObservacion((String) row.get("observacion"));
			bills.setEstado((Boolean) row.get("estado"));

			result.add(bills);

		}

		return result;

	}

	@Override
	public BillModel findBillById(long factura_id) {
		String sql = "SELECT * FROM bill WHERE id_bill = ?";
		List<BillModel> list = getJdbcTemplate().query(sql, new Object[] { factura_id }, new RowMapper<BillModel>() {
			@Override
			public BillModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				BillModel bill = new BillModel();
				bill.setFacturaId(rs.getInt("id_bill"));
				bill.setIdCustomer(rs.getInt("id_customer"));
				bill.setIva(rs.getDouble("iva"));
				bill.setSubtotal(rs.getDouble("subtotal"));
				bill.setTotal(rs.getDouble("total"));
				bill.setFecha(rs.getDate("fecha"));
				bill.setObservacion(rs.getString("observacion"));
				bill.setEstado(rs.getBoolean("estado"));
				return bill;
			}
		});

		if (list.isEmpty())
			return null;
		return list.get(0);
	}

	@Override
	public boolean updateBill(Integer id, BillModel bill) {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE bill SET total = ? WHERE id_bill =").append(id);
		getJdbcTemplate().update(sql.toString(), new Object[] { bill.getTotal() });

		return Boolean.TRUE;

	}

	@Override
	public boolean updateStateBill(Integer id, BillModel bm) {

		if (bm.getFacturaId() != null) {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE bill SET observacion =?, estado = ? WHERE id_bill= ").append(id);
			getJdbcTemplate().update(sql.toString(), new Object[] {

					bm.getObservacion(), bm.getEstado(),

			});
		}

		return Boolean.TRUE;
	}

	@Override
	public boolean deleteBill(BillModel id) {

		String sql = "DELETE FROM bill WHERE id_bill = ?";
		return getJdbcTemplate().update(sql, new Object[] { id.getFacturaId() }) > 0;

	}

	@Override
	public Integer insertAndReturn(BillModel bill) {

		final String INSERT_SQL = "INSERT INTO bill "
				+ "( id_customer,iva, subtotal, total, fecha, observacion, estado) VALUES ( ?, ?, ?, ?, ?, ?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERT_SQL, new String[] { "id_bill" });

				ps.setInt(1, bill.getIdCustomer());
				ps.setDouble(2, bill.getIva());
				ps.setDouble(3, bill.getSubtotal());
				ps.setDouble(4, bill.getTotal());
				ps.setDate(5, (Date) bill.getFecha());
				ps.setString(6, bill.getObservacion());
				ps.setBoolean(7, bill.getEstado());

				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key
	}

}
