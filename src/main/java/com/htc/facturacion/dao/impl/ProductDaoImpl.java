package com.htc.facturacion.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.model.ProductModel;

@Repository
public class ProductDaoImpl extends JdbcDaoSupport implements ProductDao {

	

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(ProductModel product) {

		String sql = "INSERT INTO product "
				+ "(id_product, product_name, description, unit_price, stock) VALUES (?, ?, ?, ?, ?)";
		return getJdbcTemplate().update(sql, new Object[] {

				product.getIdProduct(), product.getProductName(), product.getDescription(), product.getUnitPrice(),
				product.getStock() }) > 0;

	}

	@Transactional
	@Override
	public boolean inserBatch(List<ProductModel> products) {

		String sql = "INSERT INTO product "
				+ "(id_product, product_name, description, unit_price, stock) VALUES (?, ?, ?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ProductModel product = products.get(i);
				ps.setInt(1, product.getIdProduct());
				ps.setString(2, product.getProductName());
				ps.setString(3, product.getDescription());
				ps.setDouble(3, product.getUnitPrice());
				ps.setInt(3, product.getStock());

			}

			public int getBatchSize() {
				return products.size();
			}
		});

		return Boolean.TRUE;

	}

	@Override
	public List<ProductModel> loadAllProduct() {
		String sql = "SELECT * FROM product";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<ProductModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			ProductModel products = new ProductModel();
			products.setIdProduct((Integer) row.get("id_product"));
			products.setProductName((String) row.get("product_name"));
			products.setDescription((String) row.get("description"));
			products.setUnitPrice((Double) row.get("unit_price"));
			products.setStock((Integer) row.get("stock"));

			result.add(products);
		}

		return result;
	}

	@Override
	public ProductModel findProductById(long product_id) {
		String sql = "SELECT * FROM product WHERE id_product = ?";
		List<ProductModel> list = getJdbcTemplate().query(sql, new Object[] { product_id },
				new RowMapper<ProductModel>() {

					@Override
					public ProductModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
						ProductModel product = new ProductModel();
						product.setIdProduct(rs.getInt("id_product"));
						product.setProductName(rs.getString("product_name"));
						product.setDescription(rs.getString("description"));
						product.setUnitPrice(rs.getDouble("unit_price"));
						product.setStock(rs.getInt("stock"));

						return product;

					}
				});

		if (list.isEmpty())
			return null;
		return list.get(0);

	}

	@Override
	public boolean updateProduct(Integer id, ProductModel product) {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE product SET product_name = ?, description = ?, unit_price = ?, stock = ? WHERE id_product =")
				.append(id);
		return getJdbcTemplate().update(sql.toString(), new Object[] { product.getProductName(),
				product.getDescription(), product.getUnitPrice(), product.getStock()

		}) > 0;

	}

	@Override
	public boolean updateProductStock(Integer id, ProductModel product) {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE product SET  stock = ? WHERE id_product =").append(id);
		return getJdbcTemplate().update(sql.toString(), new Object[] {

				product.getStock()

		}) > 0;

	}

	@Override
	public boolean deleteProduct(ProductModel id) {

		String sql = "DELETE FROM product WHERE id_product = ?";
		return getJdbcTemplate().update(sql, new Object[] { id.getIdProduct() }) > 0;

	}

	@Override
	public Integer insertAndReturn(ProductModel product) {
		final String INSERTSQL = "INSERT INTO product "
				+ "(id_product, product_name, description, unit_price, stock) VALUES (?, ?, ?, ?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERTSQL, new String[] { "id_product" });

				ps.setInt(1, product.getIdProduct());
				ps.setString(2, product.getProductName());
				ps.setString(3, product.getDescription());
				ps.setDouble(3, product.getUnitPrice());
				ps.setInt(3, product.getStock());
				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key
	}

	@Override
	public boolean updateBatch(List<ProductModel> products) {

		String sql = "UPDATE product SET  stock = ? WHERE id_product = ?";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ProductModel product = products.get(i);

				ps.setInt(1, product.getStock());
				ps.setInt(2, product.getIdProduct());

			}

			public int getBatchSize() {
				return products.size();
			}
		});

		return Boolean.TRUE;
	}

}
