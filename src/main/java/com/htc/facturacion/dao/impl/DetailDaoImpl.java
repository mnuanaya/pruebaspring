package com.htc.facturacion.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.model.DetailModel;

@Repository
public class DetailDaoImpl extends JdbcDaoSupport implements DetailDao {

	

	@Autowired
	private DataSource dataSource;

	@PostConstruct
	private void initialize() {
		setDataSource(dataSource);
	}

	@Override
	public boolean insert(DetailModel detail) {

		String sql = "INSERT INTO detail "
				+ "(id_detail, id_bill, id_product, cantidad, subtotal) VALUES (?, ?, ?, ?, ?)";
		return getJdbcTemplate().update(sql, new Object[] {

				detail.getIdDetail(), detail.getIdBill(), detail.getIdProduct(), detail.getCantidad(),
				detail.getSubTotal(),

		}) > 0;

	}

	@Override
	public boolean inserBatch(List<DetailModel> details) {

		String sql = "INSERT INTO detail "
				+ "((id_detail, id_bill, id_product , cantidad, subtotal) VALUES (?, ?, ?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				DetailModel detail = details.get(i);
				ps.setInt(1, detail.getIdDetail());
				ps.setInt(2, detail.getIdBill());
				ps.setInt(3, detail.getIdProduct());
				ps.setInt(4, detail.getCantidad());
				ps.setDouble(5, detail.getSubTotal());

			}

			public int getBatchSize() {
				return details.size();
			}
		});

		return Boolean.TRUE;

	}

	@Override
	public List<DetailModel> loadAllDetail() {

		String sql = "SELECT * FROM detail";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql);

		List<DetailModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			DetailModel details = new DetailModel();

			details.setIdDetail((Integer) row.get("id_detail"));
			details.setIdBill((Integer) row.get("id_bill"));
			details.setIdProduct((Integer) row.get("id_product"));
			details.setCantidad((Integer) row.get("cantidad"));
			details.setSubTotal((Double) row.get("subtotal"));

			result.add(details);
		}

		return result;
	}

	@Override
	public DetailModel findDetailById(Integer detid) {
		String sql = "SELECT * FROM detail WHERE id_bill = ?";
		return getJdbcTemplate().queryForObject(sql, new Object[] { detid }, new RowMapper<DetailModel>() {
			@Override
			public DetailModel mapRow(ResultSet rs, int rwNumber) throws SQLException {
				DetailModel detail = new DetailModel();

				detail.setIdDetail(rs.getInt("id_detail"));
				detail.setIdBill(rs.getInt("id_bill"));
				detail.setIdProduct(rs.getInt("id_product"));
				detail.setCantidad(rs.getInt("cantidad"));
				detail.setSubTotal(rs.getDouble("subtotal"));

				return detail;
			}
		});
	}

	@Override
	public List<DetailModel> DetailById(Integer detid) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM detail where id_bill= ").append(detid);
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql.toString());

		List<DetailModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			DetailModel details = new DetailModel();

			details.setIdDetail((Integer) row.get("id_detail"));
			details.setIdBill((Integer) row.get("id_bill"));
			details.setIdProduct((Integer) row.get("id_product"));
			details.setCantidad((Integer) row.get("cantidad"));
			details.setSubTotal((Double) row.get("subtotal"));

			result.add(details);
		}

		return result;
	}

	@Override
	public boolean updateDetail(Integer id, DetailModel detail) {

		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE details SET id_bill = ?,  id_product = ?, cantidad = ?, subtotal = ? WHERE id_detail =")
				.append(id);
		return getJdbcTemplate().update(sql.toString(), new Object[] { detail.getIdBill(), detail.getIdProduct(),
				detail.getCantidad(), detail.getSubTotal() }) > 0;

	}

	@Override
	public boolean deleteDetail(DetailModel id) {

		String sql = "DELETE FROM detail WHERE id_detail = ?";
		return getJdbcTemplate().update(sql, new Object[] {

				id.getIdDetail() }) > 0;

	}

	@Override
	public Integer insertAndReturn(DetailModel detail) {
		final String INSERTSQL = "INSERT INTO detail "
				+ "((id_detail, id_bill, id_product , cantidad, subtotal) VALUES (?, ?, ?, ?, ?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		getJdbcTemplate().update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(java.sql.Connection arg0) throws SQLException {
				PreparedStatement ps = arg0.prepareStatement(INSERTSQL, new String[] { "id_detail" });

				ps.setInt(1, detail.getIdDetail());
				ps.setInt(2, detail.getIdBill());
				ps.setInt(3, detail.getIdProduct());
				ps.setInt(4, detail.getCantidad());
				ps.setDouble(5, detail.getSubTotal());
				return ps;
			}

		}, keyHolder);
		return (Integer) keyHolder.getKey(); // now contains the generated key
	}

	@Override
	public boolean inserBatch(List<DetailModel> details, Integer idBill) {

		String sql = "INSERT INTO detail " + "(id_bill, id_product , cantidad, subtotal) VALUES (?, ?, ?, ?)";
		getJdbcTemplate().batchUpdate(sql, new BatchPreparedStatementSetter() {
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				DetailModel detail = details.get(i);

				ps.setInt(1, idBill);
				ps.setInt(2, detail.getIdProduct());
				ps.setInt(3, detail.getCantidad());
				ps.setDouble(4, detail.getSubTotal());

			}

			public int getBatchSize() {
				return details.size();
			}
		});

		return Boolean.TRUE;
	}

	@Override
	public List<DetailModel> loadAllDetailInBill(Integer id) {
		String sql = "SELECT * FROM detail where id_bill= ?";
		List<Map<String, Object>> rows = getJdbcTemplate().queryForList(sql, new Object[] {id});

		List<DetailModel> result = new ArrayList<>();
		for (Map<String, Object> row : rows) {
			DetailModel details = new DetailModel();

			details.setIdDetail((Integer) row.get("id_detail"));
			details.setIdBill((Integer) row.get("id_bill"));
			details.setIdProduct((Integer) row.get("id_product"));
			details.setCantidad((Integer) row.get("cantidad"));
			details.setSubTotal((Double) row.get("subtotal"));

			result.add(details);
		}

		return result;
	}

}
