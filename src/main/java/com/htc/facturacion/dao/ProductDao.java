package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.ProductModel;

public interface ProductDao {

	public boolean insert(ProductModel product);

	public Integer insertAndReturn(ProductModel product);

	public boolean inserBatch(List<ProductModel> products);

	public boolean updateBatch(List<ProductModel> products);

	public List<ProductModel> loadAllProduct();

	public ProductModel findProductById(long product_id);

	public boolean updateProduct(Integer id, ProductModel product);

	public boolean updateProductStock(Integer id, ProductModel product);

	public boolean deleteProduct(ProductModel id);

}
