package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.DetailModel;

public interface DetailDao {

	public boolean insert(DetailModel detail);

	public Integer insertAndReturn(DetailModel detail);

	public boolean inserBatch(List<DetailModel> details);

	public boolean inserBatch(List<DetailModel> details, Integer idBill);

	public List<DetailModel> loadAllDetail();

	public DetailModel findDetailById(Integer detid);

	public List<DetailModel> DetailById(Integer detid);

	public boolean updateDetail(Integer id, DetailModel detail);

	public boolean deleteDetail(DetailModel id);
	
	public List<DetailModel> loadAllDetailInBill(Integer id);

}
