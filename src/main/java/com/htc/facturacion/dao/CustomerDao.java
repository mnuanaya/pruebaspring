package com.htc.facturacion.dao;

import java.util.List;

import com.htc.facturacion.model.CustomerModel;

public interface CustomerDao {

	public boolean insert(CustomerModel customer);

	public Integer insertAndReturn(CustomerModel customer);

	public boolean inserBatch(List<CustomerModel> customers);

	public List<CustomerModel> loadAllCustomer();

	public CustomerModel findCustomerById(long cusid);

	public boolean updateCustomer(Integer id, CustomerModel customer);

	public boolean deleteCustomer(CustomerModel id);

}
