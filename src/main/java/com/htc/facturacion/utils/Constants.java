package com.htc.facturacion.utils;

public final class Constants {
	public static final String IVA = "constant.iva";
	
	public static final String SUCCESS_CODE = "successCode";
	public static final String MESSAGE_SUCCESS = "messageSucces";
	
	public static final String ERROR_CODE = "errorCode";
	public static final String MESSAGE_ERROR = "messageError";
	
	public static final String NULL_CODE = "nullCode";
	public static final String MESSAGE_NULL = "messageNull";
	
	public static final String EMPTY_CODE = "emptyCode";
	public static final String MESSAGE_EMPTY = "messageEmpty";
	
	public static final String STOCK = "stock";
	public static final String MESSAGE_STOCK = "messageStock";
	
	public static final String POSITIVE_CODE = "positiveCode";
	public static final String MESSAGE_POSITIVE = "messagePositive";
	
	public static final String NOTFOUND_CODE = "notFound";
	public static final String MESSAGE_NOTFOUND = "messageNotFound";
	
	public static final String VACIO_CODE = "vaciaCode";
	public static final String MESSAGE_VACIO = "messageVaciaCode";
	
	public static final String PRODUCT_NULL_CODE = "productoNullCode";
	public static final String MESSAGE_PRODUCT_NULL = "messageProductNull";
	
	public static final String FACTURA_NULL_CODE = "facturaNullCode";
	public static final String MESSAGE_FACTURA_NULL = "messageFacturaNull";
	
	public static final String CUSTOMER_NULL_CODE = "customerNullCode";
	public static final String MESSAGE_CUSTOMER_NULL = "messageCustomerNullCode";
	
	public static final String NO_ANULAR_CODE = "noAnularCode";
	public static final String MESSAGE_NO_ANULAR = "messageNoAnular";
	
	public static final String MESSAGE_OBSERVATION_INSERT = "messageObservacionInsert";
	public static final String MESSAGE_OBSERVATION_UPDATE = "messageObservacionUpdate";
	
	public static final String MESSAGE_OBSERVATION_ANULAR = "messageObservacionAnular";
	
	
	
	
	

}
