package com.htc.facturacion.ws.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.expo.BillExpo;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.service.BillService;
import com.htc.facturacion.service.DetailService;
import com.htc.facturacion.webs.AnularBillRequest;
import com.htc.facturacion.webs.AnularBillResponse;
import com.htc.facturacion.webs.GetBillByIdRequest;
import com.htc.facturacion.webs.GetBillByIdResponse;
import com.htc.facturacion.webs.GetBillRequest;
import com.htc.facturacion.webs.GetBillResponse;
import com.htc.facturacion.webs.InsertBillRequest;
import com.htc.facturacion.webs.InsertBillResponse;

@Endpoint
public class FacturacionEndpoint {
	private static final String NAMESPACE_URI = "http://webs.facturacion.htc.com";

	@Autowired
	private BillService billService;

	@Autowired
	private DetailService detailService;

	@Autowired
	public Environment envi;

	@Autowired
	private BillExpo billExpo;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBillRequest")
	@ResponsePayload
	public GetBillResponse getBills(@RequestPayload GetBillRequest request) throws EmptyException, Exception {

		return billExpo.getBill();
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "insertBillRequest")
	@ResponsePayload
	public InsertBillResponse insertBills(@RequestPayload InsertBillRequest request)
			throws OperationException, NullObjectException, Exception {

		BillModel billModel;
		billModel = billService.beforeInsertRest(request.getIdCustomer(), request.getDetailGet());

		return billExpo.InsertBillResponse(billModel);
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getBillByIdRequest")
	@ResponsePayload
	public GetBillByIdResponse getBillById(@RequestPayload GetBillByIdRequest request)
			throws NullObjectException, Exception {

		return billExpo.getBillById(request.getId());
	}

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "anularBillRequest")
	@ResponsePayload
	public AnularBillResponse anularBill(@RequestPayload AnularBillRequest request)
			throws NullObjectException, OperationException, Exception {

		return billExpo.anularBill(request.getId());

	}

}
