package com.htc.facturacion.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.DetailModel;
import com.htc.facturacion.service.DetailService;
import com.htc.facturacion.utils.Constants;
import com.htc.facturacion.webs.Detail;

//@Service
@Component
public class DetailServiceImpl implements DetailService {
	private static final Logger log = LoggerFactory.getLogger(DetailServiceImpl.class);

	@Autowired
	public DetailDao detailDao;

	@Autowired
	private Environment env;

	@Override
	public boolean insert(DetailModel detail) throws OperationException {

		try {
			return detailDao.insert(detail);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public boolean inserBatchDetail(List<DetailModel> details) throws OperationException {

		try {
			detailDao.inserBatch(details);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}

		return Boolean.TRUE;
	}

	@Override
	public List<DetailModel> loadAllDetails() throws EmptyException {

		List<DetailModel> listDetails = detailDao.loadAllDetail();

		if (listDetails.isEmpty()) {
			throw new EmptyException(env.getProperty(Constants.MESSAGE_NULL));
		}
		return listDetails;

	}

	@Override
	public DetailModel getDetalleById(Integer detail_id) throws NullObjectException {

		DetailModel detail = detailDao.findDetailById(detail_id);

		if (detail == null) {
			throw new NullObjectException(env.getProperty(Constants.MESSAGE_NULL));
		}

		return detail;

	}

	@Override
	public boolean updateDetail(Integer id, DetailModel detail) throws OperationException {

		try {
			detailDao.updateDetail(id, detail);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public boolean deleteDetail(DetailModel id) throws OperationException {

		try {
			detailDao.deleteDetail(id);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}

		return Boolean.TRUE;
	}

	@Override
	public Integer insertAndReturn(DetailModel detail) throws NullObjectException {
		try {
			return detailDao.insertAndReturn(detail);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}

	@Override
	public DetailModel getDetail(Integer idP, Integer cantidad) throws NullObjectException {

		DetailModel detail = new DetailModel();

		detail.setIdProduct(idP);
		detail.setCantidad(cantidad);
		return detail;
	}

	@Override
	public DetailModel getDetail(List<Integer> idp, List<Integer> cantidad) throws NullObjectException {

		try {
			
			if (idp == null && cantidad == null) {
				throw new NullObjectException(Long.valueOf(env.getProperty(Constants.VACIO_CODE)),
						env.getProperty(Constants.MESSAGE_VACIO));
			}
			
			DetailModel detailModel = new DetailModel();

			for (Integer listIdp : idp) {
				detailModel.setIdProduct(listIdp);

			}

			for (Integer listCantidad : cantidad) {

				detailModel.setCantidad(listCantidad);

			}
			
			return detailModel;

		} catch (NullObjectException e) {
			log.error(e.toString());
			throw e;
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}

	}

	@Override
	public Detail castToDto(DetailModel modelDetail) {
		Detail detail = new Detail();
		detail.setIdDetail(modelDetail.getIdDetail());
		detail.setIdBill(modelDetail.getIdBill());
		detail.setIdProduct(modelDetail.getIdProduct());
		detail.setCantidad(modelDetail.getCantidad());
		detail.setSubtotal(modelDetail.getSubTotal());

		return detail;
	}

	@Override
	public DetailModel castToModel(Detail dtoDetail) throws OperationException {

		if (dtoDetail == null)
			throw new OperationException(env.getProperty(Constants.MESSAGE_VACIO));

		return new DetailModel(dtoDetail.getIdDetail(), dtoDetail.getIdBill(), dtoDetail.getIdProduct(),
				dtoDetail.getCantidad(), dtoDetail.getSubtotal());
	}

	@Override
	public List<Detail> castToDtoList(List<DetailModel> modelDetailList) {

		List<Detail> listDet = new ArrayList<>();
		for (DetailModel dModel : modelDetailList) {
			listDet.add(castToDto(dModel));
		}
		return listDet;
	}

}
