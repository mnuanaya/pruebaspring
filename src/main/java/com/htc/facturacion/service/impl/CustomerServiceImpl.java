package com.htc.facturacion.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.facturacion.dao.CustomerDao;
import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.CustomerModel;
import com.htc.facturacion.service.CustomerService;
import com.htc.facturacion.utils.Constants;
import com.htc.facturacion.webs.Customer;

//@Service
@Component
public class CustomerServiceImpl implements CustomerService {
	private static final Logger log = LoggerFactory.getLogger(CustomerServiceImpl.class);
	@Autowired
	public CustomerDao customerDao;

	@Autowired
	private Environment env;

	@Override
	public boolean insert(CustomerModel customer) throws OperationException {
		try {
			if (!customerDao.insert(customer)) {

				throw new OperationException(Long.valueOf(env.getProperty(Constants.ERROR_CODE)),
						env.getProperty(Constants.MESSAGE_ERROR));
			}
			return customerDao.insert(customer);
		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public boolean inserBatchCustomer(List<CustomerModel> customers) throws OperationException {
		try {
			customerDao.inserBatch(customers);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public List<CustomerModel> loadAllCustomer() throws EmptyException {
		List<CustomerModel> listCustomer = customerDao.loadAllCustomer();

		if (listCustomer.isEmpty()) {
			throw new EmptyException(env.getProperty(Constants.MESSAGE_NULL));
		}
		return listCustomer;
	}

	@Override
	public CustomerModel getCustomerById(long cusid) throws NullObjectException {
		CustomerModel custom = customerDao.findCustomerById(cusid);
		if (custom == null) {
			throw new NullObjectException(Long.valueOf(env.getProperty(Constants.CUSTOMER_NULL_CODE)),
					env.getProperty(Constants.MESSAGE_CUSTOMER_NULL));
		}
		return custom;
	}

	@Override
	public Customer castToDto(CustomerModel modelCustomer) {

		Customer custo = new Customer();
		custo.setIdCustomer(modelCustomer.getIdCustomer());
		custo.setNombre(modelCustomer.getNombre());
		custo.setApellido(modelCustomer.getApellido());
		return custo;
	}

	@Override
	public CustomerModel castToModel(Customer dtoCustomer) throws OperationException {
		if (dtoCustomer == null || dtoCustomer.getNombre().isEmpty() || dtoCustomer.getNombre() == null
				|| dtoCustomer.getApellido().isEmpty() || dtoCustomer.getApellido() == null) {
			throw new OperationException(env.getProperty(Constants.MESSAGE_NULL));
		}
		return new CustomerModel(dtoCustomer.getIdCustomer(), dtoCustomer.getNombre(), dtoCustomer.getApellido());
	}

	@Override
	public boolean updateCustomer(Integer id, CustomerModel customer) throws OperationException {

		try {
			customerDao.updateCustomer(id, customer);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public boolean deleteCustomer(CustomerModel id) throws OperationException {

		try {
			customerDao.deleteCustomer(id);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public Integer insertAndReturn(CustomerModel customer) throws OperationException {
		try {
			return customerDao.insertAndReturn(customer);

		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}
	}

}
