package com.htc.facturacion.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.ProductModel;
import com.htc.facturacion.service.ProductService;
import com.htc.facturacion.utils.Constants;
import com.htc.facturacion.webs.Product;

@Component
// @Service
public class ProductServiceImpl implements ProductService {

	private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Autowired
	public ProductDao productDao;
	@Autowired
	private Environment env;

	@Override
	public boolean insert(ProductModel product) throws OperationException {
		try {
			return productDao.insert(product);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public boolean inserBatchProduct(List<ProductModel> products) throws OperationException {
		try {
			productDao.inserBatch(products);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public List<ProductModel> loadAllProduct() throws EmptyException {
		List<ProductModel> listProduct = productDao.loadAllProduct();

		if (listProduct.isEmpty()) {
			throw new EmptyException(env.getProperty(Constants.MESSAGE_EMPTY));
		}
		return listProduct;
	}

	@Override
	public ProductModel getProductById(long product_id) throws NullObjectException {

		return productDao.findProductById(product_id);
	}

	@Override
	public boolean updateProduct(Integer id, ProductModel product) throws OperationException {

		try {
			productDao.updateProduct(id, product);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public boolean deleteProduct(ProductModel id) throws OperationException {

		try {
			productDao.deleteProduct(id);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}

		return Boolean.TRUE;
	}

	@Override
	public boolean updateProductStock(Integer id, ProductModel product) throws OperationException {

		try {
			productDao.updateProductStock(id, product);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public Integer insertAndReturn(ProductModel product) throws NullObjectException {
		try {
			return productDao.insertAndReturn(product);

		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;

		}
	}

	@Override
	public Product castToDto(ProductModel modelProduct) {
		Product pro = new Product();
		pro.setIdProduct(modelProduct.getIdProduct());
		pro.setProductName(modelProduct.getProductName());
		pro.setDescription(modelProduct.getDescription());
		pro.setUnitPrice(modelProduct.getUnitPrice());
		pro.setStock(modelProduct.getStock());

		return pro;
	}

	@Override
	public ProductModel castToModel(Product dtoProduct) throws OperationException {

		if (dtoProduct == null || dtoProduct.getProductName().isEmpty() || dtoProduct.getProductName() == null
				|| dtoProduct.getDescription().isEmpty() || dtoProduct.getDescription() == null) {
			throw new OperationException(env.getProperty(Constants.MESSAGE_EMPTY));
		}
		return new ProductModel(dtoProduct.getIdProduct(), dtoProduct.getProductName(), dtoProduct.getDescription(),
				dtoProduct.getUnitPrice(), dtoProduct.getStock());
	}

}
