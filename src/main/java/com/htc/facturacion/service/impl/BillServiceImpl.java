package com.htc.facturacion.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.htc.facturacion.dao.BillDao;
import com.htc.facturacion.dao.DetailDao;
import com.htc.facturacion.dao.ProductDao;
import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.model.DetailModel;
import com.htc.facturacion.model.ProductModel;
import com.htc.facturacion.service.BillService;
import com.htc.facturacion.service.CustomerService;
import com.htc.facturacion.service.DetailService;
import com.htc.facturacion.utils.Constants;
import com.htc.facturacion.webs.Bill;
import com.htc.facturacion.webs.DetailGet;

@Component
// @Service
public class BillServiceImpl implements BillService {
	private static final Logger log = LoggerFactory.getLogger(BillServiceImpl.class);
	@Autowired
	public BillDao billDao;

	@Autowired
	public CustomerService customerService;

	@Autowired
	DetailService detailService;

	@Autowired
	public ProductDao productoDao;
	@Autowired
	public DetailDao detailDao;
	@Autowired
	private Environment env;

	@Transactional
	@Override
	public boolean insert(BillModel bill) throws OperationException, NullObjectException {
		try {
			if (bill == null || bill.getDetails() == null || bill.getDetails().isEmpty()) {
				log.error("No se puede insertar factura. campos vacios");
				throw new OperationException(Long.valueOf(env.getProperty(Constants.FACTURA_NULL_CODE)),
						env.getProperty(Constants.MESSAGE_FACTURA_NULL));

			} else if (customerService.getCustomerById(bill.getIdCustomer()) == null) {
				log.error("Id customer no encontrado");
				throw new NullObjectException(Long.valueOf(env.getProperty(Constants.CUSTOMER_NULL_CODE)),
						env.getProperty(Constants.MESSAGE_CUSTOMER_NULL));
			}
			Double subtotal = 0.00;
			List<ProductModel> products = new ArrayList<>();
			for (DetailModel d : bill.getDetails()) {
				ProductModel p = productoDao.findProductById(d.getIdProduct());
				if (p == null) {
					log.error("Producto nulo");
					throw new OperationException(Long.valueOf(env.getProperty(Constants.PRODUCT_NULL_CODE)),
							env.getProperty(Constants.MESSAGE_PRODUCT_NULL));

				}

				if (d.getCantidad() < 1) {
					throw new OperationException(Long.valueOf(env.getProperty(Constants.POSITIVE_CODE)),
							env.getProperty(Constants.MESSAGE_POSITIVE));

				} else if (p.getStock() >= d.getCantidad()) {
					d.setSubTotal(p.getUnitPrice() * d.getCantidad());
					subtotal += d.getSubTotal();

					Integer stock = (p.getStock() - d.getCantidad());
					p.setStock(stock);
					productoDao.updateProductStock(p.getIdProduct(), p);
					products.add(p);

				} else {

					log.error("no hay suficiente producto");
					throw new OperationException(Long.valueOf(env.getProperty(Constants.STOCK)),
							env.getProperty(Constants.MESSAGE_STOCK));

				}
			}
			Double iva = Double.valueOf(env.getProperty("constant.iva"));

			java.util.Date fecha = new java.util.Date();
			java.sql.Date fechasql = new java.sql.Date(fecha.getTime());

			bill.setIva(subtotal * iva);
			bill.setSubtotal(subtotal);
			bill.setTotal(bill.getIva() + bill.getSubtotal());
			bill.setFecha(fechasql);
			bill.setObservacion(env.getProperty(Constants.MESSAGE_OBSERVATION_INSERT));
			bill.setEstado(true);

			Integer llave = billDao.insertAndReturn(bill);

			if (llave == null) {
				log.error("no se pudo insertar factura");
				throw new OperationException(Long.valueOf(env.getProperty(Constants.VACIO_CODE)),
						env.getProperty(Constants.MESSAGE_VACIO));

			}

			productoDao.updateBatch(products);

			return detailDao.inserBatch(bill.getDetails(), llave);

		} catch (OperationException e) {
			log.error(e.getMessage());
			throw e;

		} catch (NullObjectException e) {
			log.error(e.getMessage());
			throw e;
		} catch (Exception e) {

			log.error(e.getMessage());
			throw e;
		}

	}

	@Override
	public boolean inserBatchBill(List<BillModel> bills) throws OperationException {
		try {
			if (!billDao.inserBatch(bills)) {
				throw new OperationException(Long.valueOf(env.getProperty(Constants.NULL_CODE)), env.getProperty(Constants.MESSAGE_NULL));
			}

		} catch (OperationException e) {
			log.error(e.getMessage());
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public List<BillModel> loadAllBills() throws EmptyException {
		try {
			List<BillModel> list = billDao.loadAllBills();

			if (list.isEmpty()) {
				throw new EmptyException(Long.valueOf(env.getProperty(Constants.EMPTY_CODE)), env.getProperty(Constants.MESSAGE_EMPTY));
			}

			for (BillModel billModel : list) {
				billModel.setDetails(detailDao.loadAllDetailInBill(billModel.getFacturaId()));
			}
			return list;

		} catch (EmptyException e) {
			log.error(e.toString());
			throw e;
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}

	}

	@Override
	public BillModel getFacturaById(long fid) throws NullObjectException {
		try {
			BillModel billModel = billDao.findBillById(fid);
			if (billModel == null) {

				throw new NullObjectException(Long.valueOf(env.getProperty(Constants.NOTFOUND_CODE)),
						env.getProperty(Constants.MESSAGE_NOTFOUND));
			}
			

			billModel.setDetails(detailDao.loadAllDetailInBill(billModel.getFacturaId()));
			return billModel;
		} catch (NullObjectException e) {
			log.error(e.toString());
			throw e;
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}

	}

	@Transactional
	@Override
	public void BillNull(Integer id, BillModel billModel) throws NullObjectException {

		try {
			billModel.setEstado(false);
			billModel.setObservacion(env.getProperty(Constants.MESSAGE_OBSERVATION_ANULAR));

			List<DetailModel> detailM = detailDao.DetailById(id);

			List<DetailModel> detailMList = new ArrayList<>();
			detailMList.addAll(detailM);

			List<ProductModel> productModels = new ArrayList<>();

			for (DetailModel detailModel : detailMList) {
				ProductModel pro = productoDao.findProductById(detailModel.getIdProduct());
				pro.setIdProduct(detailModel.getIdProduct());
				pro.setStock(pro.getStock() + detailModel.getCantidad());

				productoDao.updateProductStock(pro.getIdProduct(), pro);

				productModels.add(pro);

			}

			if (!billDao.updateStateBill(billModel.getFacturaId(), billModel)) {
				throw new NullObjectException(Long.valueOf(env.getProperty(Constants.ERROR_CODE)),
						env.getProperty(Constants.MESSAGE_ERROR));
			}

		} catch (NullObjectException e) {
			log.error((e.getMessage()));
			throw e;
		} catch (Exception e) {
			log.error((e.getMessage()));
			throw e;
		}

	}

	@Override
	public Bill castToDto(BillModel modelBill) {

		Bill b = new Bill();
		b.setIdBill(modelBill.getFacturaId());
		b.setIdCustomer(modelBill.getIdCustomer());
		b.setIva(modelBill.getIva());
		b.setSubtotal(modelBill.getSubtotal());
		b.setTotal(modelBill.getTotal());
		b.setFecha(castDate(modelBill.getFecha()));
		b.setObservacion(modelBill.getObservacion());
		b.setEstado(modelBill.getEstado());

		for (DetailModel dm : modelBill.getDetails()) {
			
			b.getDetalle().add(detailService.castToDto(dm));
			
		}
	

		return b;
	}

	@Override
	public BillModel castToModel(Bill dtoBill) throws OperationException {

		if (dtoBill == null)
			throw new OperationException(env.getProperty(Constants.MESSAGE_NULL));

		Date date = dtoBill.getFecha().toGregorianCalendar().getTime();
		return new BillModel(dtoBill.getIdBill(), dtoBill.getIdCustomer(), dtoBill.getIva(), dtoBill.getSubtotal(),
				dtoBill.getTotal(), date, dtoBill.getObservacion(), dtoBill.isEstado());
	}

	@Override
	public List<Bill> castToDtoList(List<BillModel> modelBillList) {

		List<Bill> bills = new ArrayList<>();
		for (BillModel modelBill : modelBillList) {
			bills.add(castToDto(modelBill));
		}
		return bills;
	}

	public XMLGregorianCalendar castDate(Date date) {

		XMLGregorianCalendar xmlDate = null;
		GregorianCalendar gc = new GregorianCalendar();

		gc.setTime(date);

		try {
			xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (Exception e) {
			log.error("No se pudo castear fecha");
		}
		return xmlDate;
	}

	@Override
	public boolean updateBill(Integer id, BillModel bill) {

		try {
			billDao.updateBill(id, bill);

		} catch (Exception e) {
			log.error((e.getMessage()));
			throw e;
		}
		return Boolean.TRUE;

	}

	@Override
	public boolean deleteBill(BillModel bill) {

		try {
			billDao.deleteBill(bill);

		} catch (Exception e) {
			log.error((e.getMessage()));
			throw e;
		}
		return Boolean.TRUE;
	}

	@Override
	public Integer insertAndReturn(BillModel bill) {
		try {
			return billDao.insertAndReturn(bill);

		} catch (Exception e) {
			log.error((e.getMessage()));
			throw e;
		}

	}

	@Override
	public BillModel beforeInsertRest(Integer id, List<DetailGet> listGet) throws NullObjectException {
		try {
			
			if(listGet.isEmpty()) {
				throw new NullObjectException(Long.valueOf(env.getProperty(Constants.NULL_CODE)), env.getProperty(Constants.MESSAGE_NULL));
			}
			
			BillModel billModel = new BillModel();
			List<DetailModel> detalleList = new ArrayList<>();

			for (DetailGet detlist : listGet) {
				detlist.getCantidad();
				detlist.getIdProduct();

				detalleList.add(detailService.getDetail(detlist.getIdProduct(), detlist.getCantidad()));
			}

			billModel.setIdCustomer(id);
			billModel.setDetails(detalleList);

			
			return billModel;

		} catch (NullObjectException e) {
			throw e;
		} catch (Exception e) {

			throw e;
		}
	}

}
