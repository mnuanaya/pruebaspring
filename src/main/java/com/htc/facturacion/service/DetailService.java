package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.DetailModel;
import com.htc.facturacion.webs.Detail;

public interface DetailService {

	public boolean insert(DetailModel detail) throws OperationException;

	public Integer insertAndReturn(DetailModel detail) throws NullObjectException;

	public boolean inserBatchDetail(List<DetailModel> details) throws OperationException;

	public List<DetailModel> loadAllDetails() throws EmptyException;

	public DetailModel getDetalleById(Integer detail_id) throws NullObjectException;

	public DetailModel getDetail(Integer idP, Integer cantidad) throws NullObjectException;

	public DetailModel getDetail(List<Integer> idp, List<Integer> cantidad) throws NullObjectException;

	public boolean updateDetail(Integer id, DetailModel detail) throws OperationException;

	public boolean deleteDetail(DetailModel id) throws OperationException;

	// cast
	public Detail castToDto(DetailModel modelDetail);

	public DetailModel castToModel(Detail dtoDetail) throws OperationException;

	public List<Detail> castToDtoList(List<DetailModel> modelDetailList);
}
