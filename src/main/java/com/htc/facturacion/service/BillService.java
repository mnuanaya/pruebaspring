package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.BillModel;
import com.htc.facturacion.webs.Bill;
import com.htc.facturacion.webs.DetailGet;

public interface BillService {

	public boolean insert(BillModel bill) throws OperationException, NullObjectException;

	public Integer insertAndReturn(BillModel bill) throws OperationException;

	public boolean inserBatchBill(List<BillModel> bills) throws OperationException;

	public List<BillModel> loadAllBills() throws EmptyException;

	public BillModel getFacturaById(long fid) throws NullObjectException;

	public boolean updateBill(Integer id, BillModel bill) throws OperationException;

	public boolean deleteBill(BillModel bill) throws OperationException;

	public void BillNull(Integer id, BillModel billModel) throws NullObjectException;
	
	public BillModel beforeInsertRest(Integer id, List<DetailGet> listGet)throws NullObjectException;

	// cast
	public Bill castToDto(BillModel modelBill);

	public BillModel castToModel(Bill dtoBill) throws OperationException;

	public List<Bill> castToDtoList(List<BillModel> modelBillList);

}
