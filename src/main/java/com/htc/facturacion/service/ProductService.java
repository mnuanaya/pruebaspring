package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.ProductModel;
import com.htc.facturacion.webs.Product;

public interface ProductService {

	public boolean insert(ProductModel product) throws OperationException;

	public Integer insertAndReturn(ProductModel product) throws NullObjectException;

	public boolean inserBatchProduct(List<ProductModel> products) throws OperationException;

	public List<ProductModel> loadAllProduct() throws EmptyException;

	public ProductModel getProductById(long product_id) throws NullObjectException;

	public boolean updateProduct(Integer id, ProductModel product) throws OperationException;

	public boolean updateProductStock(Integer id, ProductModel product) throws OperationException;

	public boolean deleteProduct(ProductModel id) throws OperationException;

	// cast
	public Product castToDto(ProductModel modelProduct);

	public ProductModel castToModel(Product dtoProduct) throws OperationException;

}
