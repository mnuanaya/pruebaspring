package com.htc.facturacion.service;

import java.util.List;

import com.htc.facturacion.exception.EmptyException;
import com.htc.facturacion.exception.NullObjectException;
import com.htc.facturacion.exception.OperationException;
import com.htc.facturacion.model.CustomerModel;
import com.htc.facturacion.webs.Customer;

public interface CustomerService {

	public boolean insert(CustomerModel customer) throws OperationException;

	public Integer insertAndReturn(CustomerModel customer) throws OperationException;

	public boolean inserBatchCustomer(List<CustomerModel> customers) throws OperationException;

	public List<CustomerModel> loadAllCustomer() throws EmptyException;

	public CustomerModel getCustomerById(long cusid) throws NullObjectException;

	public boolean updateCustomer(Integer id, CustomerModel customer) throws OperationException;

	public boolean deleteCustomer(CustomerModel id) throws OperationException;

	// cast
	public Customer castToDto(CustomerModel modelCustomer);

	public CustomerModel castToModel(Customer dtoCustomer) throws OperationException;

}
