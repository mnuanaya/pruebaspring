package com.htc.facturacion.model;

import java.io.Serializable;

public class ProductModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idProduct;
	private String productName;
	private String description;
	private double unitPrice;
	private Integer stock;

	public ProductModel() {

	}

	public ProductModel(String productName, String description, double unitPrice, Integer stock) {
		super();
		this.productName = productName;
		this.description = description;
		this.unitPrice = unitPrice;
		this.stock = stock;
	}

	public ProductModel(Integer idProduct, String productName, String description, double unitPrice, Integer stock) {
		this.idProduct = idProduct;
		this.productName = productName;
		this.description = description;
		this.unitPrice = unitPrice;
		this.stock = stock;

	}

	public ProductModel(Integer idProduct) {

		this.idProduct = idProduct;
	}

	public Integer getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Product [idProduct=" + idProduct + ", productName=" + productName + ", description=" + description
				+ ", unitPrice=" + unitPrice + ", stock=" + stock + "]";
	}

}
