package com.htc.facturacion.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class BillModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Integer facturaId;
	private Integer idCustomer;
	private double iva;
	private double subtotal;
	private double total;
	private Date fecha;
	private String observacion;
	private Boolean estado;

	private List<DetailModel> details;

	
	public BillModel() {

	}

	
	
	

	public BillModel(Integer idCustomer, double iva, Date fecha, List<DetailModel> details) {
		super();
		this.idCustomer = idCustomer;
		this.iva = iva;
		this.fecha = fecha;
		this.details = details;
		
	}





	public BillModel(Integer facturaId, Integer idCustomer, double iva, double subtotal, double total, Date fecha,
			List<DetailModel> details) {

		this.facturaId = facturaId;
		this.idCustomer = idCustomer;
		this.iva = iva;
		this.subtotal = subtotal;
		this.total = total;
		this.fecha = fecha;
		this.details = details;
	}


	

	public BillModel(Integer facturaId, Integer idCustomer, double iva, double subtotal, double total,
			Date fecha, String observacion, Boolean estado) {
		super();
		this.facturaId = facturaId;
		this.idCustomer = idCustomer;
		this.iva = iva;
		this.subtotal = subtotal;
		this.total = total;
		this.fecha = fecha;
		this.observacion=observacion;
		this.estado=estado;
	}





	public BillModel(Integer facturaId) {
		this.facturaId = facturaId;
	}
	

	public Integer getFacturaId() {
		return facturaId;
	}

	public void setFacturaId(Integer facturaId) {
		this.facturaId = facturaId;
	}

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getSubtotal() {
		return subtotal;
	}

	public void setSubtotal(double subtotal) {
		this.subtotal = subtotal;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	

	public String getObservacion() {
		return observacion;
	}


	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Boolean getEstado() {
		return estado;
	}


	public void setEstado(Boolean estado) {
		this.estado = estado;
	}


	public List<DetailModel> getDetails() {
		return details;
	}

	public void setDetails(List<DetailModel> details) {
		this.details = details;
	}





	@Override
	public String toString() {
		return "BillModel [facturaId=" + facturaId + ", idCustomer=" + idCustomer + ", iva=" + iva + ", subtotal="
				+ subtotal + ", total=" + total + ", fecha=" + fecha + ", observacion=" + observacion + ", estado="
				+ estado + "]";
	}



}
