package com.htc.facturacion.model;

import java.io.Serializable;

public class DetailModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idDetail;
	private Integer idBill;
	private Integer idProduct;
	private Integer cantidad;
	private Double subTotal;

	public DetailModel() {

	}

	public DetailModel(Integer cantidad, Double subTotal) {
		super();
		this.cantidad = cantidad;
		this.subTotal = subTotal;

	}

	public DetailModel(Integer idProduct, Integer cantidad) {
		super();
		this.idProduct = idProduct;
		this.cantidad = cantidad;
	}

	public DetailModel(Integer idDetail, Integer idBill, Integer idProduct, Integer cantidad, Double subTotal) {

		this.idDetail = idDetail;
		this.idBill = idBill;
		this.idProduct = idProduct;
		this.cantidad = cantidad;
		this.subTotal = subTotal;
	}

	public DetailModel(Integer idDetail) {

		this.idDetail = idDetail;
	}

	public Integer getIdDetail() {
		return idDetail;
	}

	public void setIdDetail(Integer idDetail) {
		this.idDetail = idDetail;
	}

	public Integer getIdBill() {
		return idBill;
	}

	public void setIdBill(Integer idBill) {
		this.idBill = idBill;
	}

	public Integer getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	@Override
	public String toString() {
		return "Detail [idDetail=" + idDetail + ", idBill=" + idBill + ", idProduct=" + idProduct + ", cantidad="
				+ cantidad + ", subTotal=" + subTotal + "]";
	}

}
