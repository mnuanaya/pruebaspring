package com.htc.facturacion.model;

import java.io.Serializable;

public class CustomerModel implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer idCustomer;
	private String nombre;
	private String apellido;

	public CustomerModel() {

	}

	public CustomerModel(Integer idCustomer, String nombre, String apellido) {
		this.idCustomer = idCustomer;
		this.nombre = nombre;
		this.apellido = apellido;
	}

	public CustomerModel(Integer idCustomer) {

		this.idCustomer = idCustomer;
	}

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@Override
	public String toString() {
		return "Customer [idCustomer=" + idCustomer + ", nombre=" + nombre + ", apellido=" + apellido + "]";
	}

}
